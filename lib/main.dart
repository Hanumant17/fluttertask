import 'package:flutter/material.dart';
import 'package:task_app/utils/locator.dart';
import 'package:task_app/utils/share_prefs.dart';
import './screens/homepage.dart';
import './screens/login.dart';
import './screens/otppage.dart';

main() async{
  WidgetsFlutterBinding.ensureInitialized();
  setupLocator();
  await locator<SharedPrefs>().init();
  runApp(const MyApp());
}
class MyApp extends StatelessWidget {
   const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    SharedPrefs prefs = new SharedPrefs();
    return MaterialApp(
      debugShowCheckedModeBanner: false,
        home: prefs.isLogin ? HomePage() : Login(),
    );
  }
}
