import 'package:sqflite/sqflite.dart';
import 'package:path/path.dart';
import './task_model.dart';

class DatabaseConnect{
  Database? _database;

  Future<Database> get database async{
    final dbpath = await getDatabasesPath();
    const dbname = 'task.db';
    final path = join(dbpath, dbname);

    _database =await openDatabase(path, version: 1, onCreate: _createDB);
     return _database!;
  }

  Future<void> _createDB(Database db, int version) async{
    await db.execute('''
    CREATE TABLE task(
    id INTEGER PRIMARY KEY AUTOINCREMENT,
    title TEXT
    )
     ''');
  }
  Future<void> insertTask(Task task) async{
    final db = await database;
    await db.insert('task', task.toMap(), conflictAlgorithm: ConflictAlgorithm.replace,);
  }
  Future<void> deleteTask(Task task) async{
    final db = await database;
    await db.delete('task', where: 'id == ?',whereArgs: [task.id],);
  }
  Future<List<Task>> getTask() async {
    final db = await database;
    List<Map<String, dynamic>> items = await db.query(
      'task', orderBy: 'id DESC',);
    return List.generate(items.length,
        (i) => Task(
            id: items[i]['id'],
            title: items[i]['title']),
        );
  }

}

