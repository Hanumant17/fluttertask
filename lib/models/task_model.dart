class Task{
  int? id;
  final String title;

  Task({
    this.id,
    required this.title,
});
  Map<String, dynamic> toMap() {
    return {
      'id' : id,
      'title' : title
    };
  }
  @override
  String toString() {
    // TODO: implement toString
    return 'Todo(id : $id, title : $title)';
  }
}