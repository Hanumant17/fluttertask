import 'package:flutter/material.dart';
import 'package:task_app/models/db_model.dart';
import 'package:task_app/models/task_model.dart';
import '../widgets/user_input.dart';
import '../widgets/task_card.dart';
import '../widgets/task_list.dart';
import './profilepage.dart';

class HomePage extends StatefulWidget {

  const HomePage({Key? key}) : super(key: key);

  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  var db = DatabaseConnect();
  void addItem(Task task) async{
    await db.insertTask(task);
    setState(() {});
  }

  void deleteItem(Task task) async {
    await db.deleteTask(task);
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    Future<bool> showExitPopup() async {
      return await showDialog( //show confirm dialogue
        //the return value will be from "Yes" or "No" options
        context: context,
        builder: (context) => AlertDialog(
          title: Text('Exit App'),
          content: Text('Do you want to exit an App?'),
          actions:[
            ElevatedButton(
              onPressed: () => Navigator.of(context).pop(false),
              //return false when click on "NO"
              child:Text('No'),
            ),

            ElevatedButton(
              onPressed: () => Navigator.of(context).pop(true),
              //return true when click on "Yes"
              child:Text('Yes'),
            ),

          ],
        ),
      )??false; //if showDialouge had returned null, then return false
    }

    return WillPopScope(
        onWillPop: showExitPopup,
    child: Scaffold(
      backgroundColor: const Color(0xFFF5EBFF),
      drawer: new Drawer(
        child: new ListView(
          children: <Widget>[
            new ListTile(
              title: new Text("Home Page"),
              onTap: () {
                Navigator.of(context).push(MaterialPageRoute(builder: (context)=>HomePage()));
              },
            ),
            new ListTile(
              title: new Text("Profile Page"),
              onTap: () {
                Navigator.of(context).push(MaterialPageRoute(builder: (context)=>ProfilePge()));
              },
            )
          ],
        ),
      ),
      appBar: AppBar(
        title: const Text('Task app'),
      ),
      body: Column(
        children: [
          // TaskCard(),
          TaskList(insertFunction: addItem, deleteFunction: deleteItem,),
          UserInput(
            insertFunction: addItem,
          ),
        ],
      ),
    ),
        // onWillPop: () async {
        //   return false;
        // });
    );
  }
}
