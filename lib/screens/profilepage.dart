import 'package:flutter/material.dart';
import 'package:task_app/widgets/profilecard.dart';
import 'package:task_app/widgets/profiledetailscard.dart';

class ProfilePge extends StatelessWidget {
  // const ProfilePge({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: const Text('Task app'),
        ),
        body: Center(
            child:Container(
              width: double.infinity,
              decoration: BoxDecoration(
                color: Colors.white,
              ),

              child: Column(
                children: <Widget>[
                  SizedBox(height: 20,),
                  Container(
                    decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.circular(10)
                    ),
                    child: ProfileCard(),
                  ),
                  SizedBox(height: 20,),
                  // Header(),
                  Expanded(child: Container(
                    decoration: BoxDecoration(
                        color: Colors.black87,
                        borderRadius: BorderRadius.only(
                          topLeft: Radius.circular(40),
                          topRight: Radius.circular(40),
                        )
                    ),

                    child: ProfileDetailsCard(),

                  ),
                  ),

                ],
              ),
            )
        )
    );
  }
}
