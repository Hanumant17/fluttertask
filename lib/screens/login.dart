import 'package:flutter/material.dart';
import 'package:task_app/screens/otppage.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../utils/share_prefs.dart';
class Login extends StatelessWidget {
  // BuildContext context;
  // @override
  // void initState() {
  //   // TODO: implement initState
  //   // var initState = super.initState();
  //   startTimer();
  // }

  const Login({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    SharedPrefs prefs = new SharedPrefs();

    // await prefs.init();
    TextEditingController email = TextEditingController();
    TextEditingController pass = TextEditingController();
    return Scaffold(
      appBar: AppBar(
        title: Text("Login Page"),
      ),

      body: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Padding(
            padding: const EdgeInsets.fromLTRB(30, 0, 30, 0),
            child: TextField(
              onChanged: (text)
              {},
              decoration: InputDecoration(
                hintText: 'Enter your Phone Number',
                hintStyle: TextStyle(color: Colors.grey),
                enabledBorder: OutlineInputBorder(
                  borderRadius: new BorderRadius.circular(10.0),
                  borderSide: BorderSide(
                      color: Colors.black
                  ),
                ),
                focusedBorder: OutlineInputBorder(
                    borderRadius: new BorderRadius.circular(10.0),
                    borderSide: BorderSide(
                        color: Colors.blue
                    )
                ),
                isDense: true,                      // Added this
                contentPadding: EdgeInsets.fromLTRB(10, 20, 10, 10),
              ),
              cursorColor: Colors.black,
              style: TextStyle(color: Colors.black),
            ),
          ),
          SizedBox(
            height: 20,
          ),
          RaisedButton(
            color: Colors.blue,
            onPressed: () {
              Navigator.of(context).push(MaterialPageRoute(builder: (context)=>OTPPage()));
            },
            child: Text("Send OTP"),
          ),

        ],
      ),

    );


  }

  // void startTimer() {
  //
  //   navigateUser();
  // }
  // void navigateUser() async{
  //   SharedPreferences prefs = await SharedPreferences.getInstance();
  //   var status = prefs.getBool('isLoggedIn') ?? false;
  //   print(status);
  //   if (status) {
  //     Navigator.of(context).push(MaterialPageRoute(builder: (context)=>OTPPage()));
  //   } else {
  //     Navigator.of(context).push(MaterialPageRoute(builder: (context)=>OTPPage()));
  //   }
  // }
}

