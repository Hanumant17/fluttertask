import 'package:flutter/material.dart';
import 'package:task_app/screens/homepage.dart';
// import 'shar';
import 'package:shared_preferences/shared_preferences.dart';
import '../utils/share_prefs.dart';
class OTPPage extends StatelessWidget {
  const OTPPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    TextEditingController email = TextEditingController();
    TextEditingController pass = TextEditingController();
    return Scaffold(
      appBar: AppBar(
        title: Text("OTP Page"),
      ),
      body: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Padding(
            padding: const EdgeInsets.fromLTRB(30, 0, 30, 0),
            child: TextField(
              onChanged: (text)
              {},
              decoration: InputDecoration(
                hintText: 'Enter your OTP',
                hintStyle: TextStyle(color: Colors.grey),
                enabledBorder: OutlineInputBorder(
                  borderRadius: new BorderRadius.circular(10.0),
                  borderSide: BorderSide(
                      color: Colors.black
                  ),
                ),
                focusedBorder: OutlineInputBorder(
                    borderRadius: new BorderRadius.circular(10.0),
                    borderSide: BorderSide(
                        color: Colors.blue
                    )
                ),
                isDense: true,                      // Added this
                contentPadding: EdgeInsets.fromLTRB(10, 20, 10, 10),
              ),
              cursorColor: Colors.black,
              style: TextStyle(color: Colors.black),
            ),
          ),
          SizedBox(
            height: 20,
          ),
          RaisedButton(
            color: Colors.blue,
            onPressed: () async {
              // SharedPreferences prefs = await SharedPreferences.getInstance();
              // prefs.setBool("isLoggedIn", true);
              // var prefs;
              // final prefs = SharedPrefs;
              SharedPrefs prefs = new SharedPrefs();
              prefs.token = 'LoggedIn';
              Navigator.pushAndRemoveUntil<dynamic>(
                context,
                MaterialPageRoute<dynamic>(
                  builder: (BuildContext context) => HomePage(),
                ),
                    (route) => false,//if you want to disable back feature set to false
              );},
            child: Text("Login"),
          ),

        ],
      ),
    );
  }
}
