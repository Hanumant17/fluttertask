
import 'package:get_it/get_it.dart';

import 'share_prefs.dart';

GetIt locator = GetIt.instance;

void setupLocator() {
  locator.registerLazySingleton(() => SharedPrefs());
}
