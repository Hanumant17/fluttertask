import 'package:flutter/material.dart';
import '../models/db_model.dart';
import './task_card.dart';
class TaskList extends StatelessWidget {
  var db = DatabaseConnect();
  final Function insertFunction;
  final Function deleteFunction;
  TaskList({required this.insertFunction, required this.deleteFunction, Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Expanded(
        child: FutureBuilder(
          future: db.getTask(),
          initialData: const [],
          builder: (BuildContext context, AsyncSnapshot<List> snapshot){
            var data = snapshot.data;
            var datalength = data!.length;
            return datalength == 0
                ? const Center(
              child: Text('no'),
            ) : ListView.builder(itemCount: datalength, itemBuilder: (context, i) => TaskCard(
                id: data[i].id,
                title: data[i].title,
                insertFunction: insertFunction,
                deleteFunction: deleteFunction,
            ),
            );
          },
        ),
    );
  }
}
