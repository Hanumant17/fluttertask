import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

import 'package:shared_preferences/shared_preferences.dart';
import 'package:task_app/screens/login.dart';
class ProfileDetailsCard extends StatelessWidget {
  const ProfileDetailsCard({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Padding(padding: EdgeInsets.all(10),

      child: Column(
        children: <Widget>[
          SizedBox(height: 10,),
          Text(
            "Skill",
            style: TextStyle(color: Colors.white, fontSize: 18, fontWeight: FontWeight.bold),
          ),
          SizedBox(height: 10,),
          Container(
            child: buildIcons(),
          ),
          SizedBox(height: 20,),
      Container(
        child: TextField(
          decoration: InputDecoration(
              hintText: "Profession",
              hintStyle: TextStyle(color: Colors.grey),
              border: InputBorder.none
          ),
        ),
      ),
          // SizedBox(height: 10,),
          Container(
            decoration: BoxDecoration(
                border: Border(
                    bottom: BorderSide(color: Colors.grey)
                )
            ),
            child: TextField(
              decoration: InputDecoration(
                  hintText: "Flutter Developer",
                  hintStyle: TextStyle(color: Colors.grey, fontSize: 18),
                  border: InputBorder.none
              ),
            ),
          ),
          // Button(),
          SizedBox(height: 10,),
          // Container(
          //   decoration: BoxDecoration(
          //       border: Border(
          //       )
          //   ),
          //   child: TextField(
          //     decoration: InputDecoration(
          //         hintText: "Experience",
          //         hintStyle: TextStyle(color: Colors.grey),
          //         border: InputBorder.none
          //     ),
          //   ),
          // ),
          // const SizedBox(width: 10),
          GestureDetector(
            onTap: () async {
              SharedPreferences prefs = await SharedPreferences.getInstance();
              prefs.clear();
              Navigator.pushAndRemoveUntil<dynamic>(
                context,
                MaterialPageRoute<dynamic>(
                  builder: (BuildContext context) => Login(),
                ),
                    (route) => false,//if you want to disable back feature set to false
              );
              },
            child: Container(
              color: Colors.black87,
              padding: const EdgeInsets.symmetric(horizontal: 50, vertical: 15),
              child: const Text(
                'Logout',
                style: TextStyle(
                  color: Colors.white,
                  fontWeight: FontWeight.bold,
                ),
              ),
            ),
          )
        ],
      ),
    );
  }

  Widget buildIcons() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        buildSocialIcon(FontAwesomeIcons.apple),
        const SizedBox(width: 20,),
        buildSocialIcon(FontAwesomeIcons.amazon),
        const SizedBox(width: 20,),
        buildSocialIcon(FontAwesomeIcons.facebook),
        const SizedBox(width: 20,),
        buildSocialIcon(FontAwesomeIcons.iceCream),
      ],
    );
  }

  Widget buildSocialIcon(IconData icon) => Container(
    decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.only(
          topLeft: Radius.circular(20),
          topRight: Radius.circular(20),
          bottomLeft: Radius.circular(20),
          bottomRight: Radius.circular(20),

        )
    ),
    child: Center(child: Icon(icon, size: 55)),
  );
}
