import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
class ProfileCard extends StatelessWidget {
  const ProfileCard({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      // padding: const EdgeInsets.symmetric(horizontal: 5, vertical: 8),
      // color: const Color(0xFFDAB5FF),
      child: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 100, vertical: 8),
        child: Column(
          children: <Widget>[
            SizedBox(
              width: 150.0,
              height: 150.0,
              child: Image.asset(
                "assets/profile.png",
                fit: BoxFit.fill,
              ),
            ),
            SizedBox(height: 10,),
            Text("Hanumantappa", style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold),),
            SizedBox(height: 5,),
            Text("Flutter Developer", style: TextStyle(fontSize: 14, fontWeight: FontWeight.normal),),
            SizedBox(height: 25,),
            buildIcons(),
          ]
        ),
      ),
    );
  }

  Widget buildIcons() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        buildSocialIcon(FontAwesomeIcons.slack),
        const SizedBox(width: 12,),
        buildSocialIcon(FontAwesomeIcons.github),
        const SizedBox(width: 12,),
        buildSocialIcon(FontAwesomeIcons.twitter),
        const SizedBox(width: 12,),
        buildSocialIcon(FontAwesomeIcons.linkedin),
      ],
    );
  }

  Widget buildSocialIcon(IconData icon) => CircleAvatar(
    radius: 15,
    child: Center(child: Icon(icon, size: 22)),
  );
}
