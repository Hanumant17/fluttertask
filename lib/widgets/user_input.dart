import 'package:flutter/material.dart';
import 'package:task_app/models/task_model.dart';

class UserInput extends StatelessWidget {
  var textController = TextEditingController();
  final Function insertFunction;

  UserInput({required this.insertFunction, Key? key}) : super(key: key);


  @override
  Widget build(BuildContext context) {
    return Container(
        padding: const EdgeInsets.symmetric(horizontal: 5, vertical: 8),
        color: const Color(0xFFDAB5FF),
        child: Row(
           children: [
             Expanded(
                 child: Container(
                   padding: const EdgeInsets.symmetric(horizontal: 5),
                   child: TextField(
                     controller: textController,
                     decoration: const InputDecoration(
                       hintText: 'Add new Item',
                       border: InputBorder.none,

                     ),
                   ),
                 ),),
             const SizedBox(width: 10),
             GestureDetector(
               onTap: () {
                 var myTask = Task(title: textController.text);
                 insertFunction(myTask);
               },
               child: Container(
                 color: Colors.red,
                 padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 15),
                 child: const Text(
                   'Add',
                   style: TextStyle(
                     color: Colors.white,
                     fontWeight: FontWeight.bold,
                 ),
                 ),
               ),
             )
           ],
        ),
    );
  }
}
