import 'package:flutter/material.dart';
import 'package:task_app/models/task_model.dart';
class TaskCard extends StatefulWidget {
  final int id;
  final String title;
  final Function insertFunction;
  final Function deleteFunction;
  const TaskCard(
      {required this.id,
        required this.title,
        required this.insertFunction,
        required this.deleteFunction,
        Key? key}) : super(key: key);

  @override
  _TaskCardState createState() => _TaskCardState();
}

class _TaskCardState extends State<TaskCard> {
  @override
  Widget build(BuildContext context) {
    var anotherTask = Task(id: widget.id, title: widget.title);
    return Card(
      child: Row(
        children: [
          Expanded(
              child: Column(
                children: [
                  Text(
                    widget.title,
                    style: const TextStyle(
                      fontSize: 18,
                      fontWeight: FontWeight.bold,
                    ),),
                ],
              ),
          ),
          IconButton(
              onPressed: () {
                widget.deleteFunction(anotherTask);
              },
              icon: const Icon(Icons.close),
          ),
        ],
      ),
    );
  }
}
